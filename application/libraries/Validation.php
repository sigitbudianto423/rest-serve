<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;
use \Firebase\JWT\JWT;

class Validation{

    public function ValidationToken()
    {
        $ci = &get_instance();
        $token = $ci->input->get_request_header('key');
        $key = "example_key";
        try {
        $decoded = JWT::decode($token, $key, array('HS256'));
           return $decoded;
        } catch (exception $e) {
            $ci->response( [
                'status' => false,
                'message' => 'Token Belum Terdaftar'
            ], 404 );
        }
    }

  
}