<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;
//use \Firebase\JWT\JWT;

class Hadiah extends RestController {
    function __construct(){
        parent::__construct();

        $this->load->library('validation');
        $this->validation->validationToken();
        $this->load->model('m_hadiah');
    }



     public function index_get($id = null){
        
    	$idClient = $this->validation->validationToken()->id_client;
        $hadiah = $this->m_hadiah->getHadiah($id, $idClient);
        if ($hadiah) {
           $this->response( [
                'status' => true,
                'message' => 'Hadiah Berhasi ditemukan',
                'data' => $hadiah
                ], 200);
        } else {
            $this->response( [
                'status' => false,
                'message' => 'Hadiah Tidak ditemukan'
                ], 404);
        }
    }

    public function index_post(){

    	$config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $nama_image = $_FILES['gambar']['name'];
        $nama_hadiah = $this->input->post('nama_hadiah');
        $point = $this->input->post('point');
        $deskripsi = $this->input->post('deskripsi');
        $idClient = $this->validation->validationToken()->id_client;
        

        $this->form_validation->set_rules('nama_hadiah', 'Nama Hadiah', 'required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
        $this->form_validation->set_rules('point', 'Point', 'required|numeric');
     	$this->form_validation->set_rules('gambar', 'Gambar', 'required');



        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('gambar'))
        {
            $this->response( [
                'status' => false,
                'message' => 'Hadiah Gagal di Upload'
            ], 404 );
        }
        else
        {
             $data= [
                "nama_hadiah" => $nama_hadiah,
                'point' => $point,
                'deskripsi' => $deskripsi,
                'id_client' => $idClient,
                'gambar' => $nama_image
            ];
            $this->db->insert('tbl_hadiah',$data);
            $this->response( [
                'status' => true,
                'message' => 'Hadiah berhasil di Upload'
            ], 200 );
        }

    }

	    	public function changeUploadImage($gambar, $gambarLama)
			    {
			    	$config['upload_path']          = './uploads/';
			        $config['allowed_types']        = 'gif|jpg|png';
			        $this->load->library('upload', $config);

			        if( ! $this->upload->do_upload('gambar')) 	{
			        	return $gambarLama;

			        }else{
			        	//unlink($gambarLama);
			        	return $this->upload->data('file_name');
			        }

			    }


     		public function index_put(){

        	 	$this->form_validation->set_data($this->put());

		        $this->form_validation->set_rules('nama_hadiah', 'Nama Hadiah', 'required');
		        $this->form_validation->set_rules('point', 'Point', 'required|numeric');
		        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
		       

		        if ($this->form_validation->run() == false) {
			            $this->response( [
				                'status' => false,
				                'message' => $this->form_validation->error_array()
			           			 ], 404 );
			        }

			        $id = $this->put('id_hadiah');
			        $gambar = $this->put('gambar');

			        $idClient = $this->validation->validationToken()->id_client;

		        if ($this->m_hadiah->cekId($id, $idClient) == 0) {
		           $this->response( [
			                'status' => false,
			                'message' => 'Id Tidak Ditemukan'
		           			 ], 404 );
		        }

			        $cekId = $this->m_hadiah->getHadiah($id, $idClient);			  
        			$gambarLama = $cekId->gambar;

        		 $data = [			       
			            'nama_hadiah' => $this->put('nama_hadiah'),
			            'deskripsi' => $this->put('deskripsi'),
			            'point' => $this->put('point'),
			            'gambar' => $this->changeUploadImage($gambar, $gambarLama)
			        ];


		         if ( ! $this->m_hadiah->updateHadiah($id, $data) == True) {
		            $this->response( [
			                'status' => true,
			                'message' => 'Hadiah Berhasil di Edit'
		           			 ], 200);
		        }else{
			        	$this->response( [
				                'status' => false,
				                'message' => 'Hadiah Gagal di Edit'
			           			 ], 404 );
		        	}
    		}




    		public function index_delete(){
		        
		        $id = $this->delete('id_hadiah');
		        $idClient = $this->validation->validationToken()->id_client;

		        if ($this->m_hadiah->cekId($id, $idClient) == 0) {
		           	$this->response( [
				                'status' => false,
				                'message' => 'Hadiah Gagal di di Hapus'
			           			 ], 404 );
	        	}else{

			        $this->m_hadiah->deleteHadiah($id, $idClient);

			        $this->response( [
					                'status' => false,
					                'message' => 'Hadiah Berhasil di Hapus'
				           			 ], 404 );
	    			}    	
		    
    		}






        
        function tukar_hadiah_post(){
            $id = $this->input->post('id_hadiah');
            $user = $this->input->post('id_user');
            $id_user = $this->db->get_where('tbl_point',array('id_user'=>$user))->row_array();
            $id_hadiah = $this->db->get_where('tbl_hadiah',array('id_hadiah'=>$id))->row_array();
            
            if($id_user == 0){
                 $this->response( [
                            'status' => false,
                            'message' => 'Id tidak ada'
                             ], 404 );
                    }  

            if($id_user['total_point'] > $id_hadiah['point'] ){
                
                $data = ['id_user'=>$id_user['id_user'],
                'id_hadiah' => $id_hadiah['id_hadiah']                
                     ];
                $this->db->insert('tbl_transaksi_hadiah', $data);

                $jumlah = $id_user['total_point'] - $id_hadiah['point'];
                $this->db->update('tbl_point', ['total_point' => $jumlah]);
                
                $this->response( [
                            'status' => true,
                            'message' => 'Terimah Kasih'
                             ], 200 );

                    }else{
                        $this->response( [
                            'status' => false,
                            'message' => 'maaf point anda kurang'
                             ], 404 );
                    }




            }











}    