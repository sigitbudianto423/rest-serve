<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;
//use \Firebase\JWT\JWT;

class Transaksi extends RestController {
    function __construct(){
        parent::__construct();

        $this->load->library('validation');
        $this->validation->validationToken();
        $this->load->model('m_produk');
        $this->load->model('m_transaksi');
    }


    function index_get($id = null){
    	//$idClient = $this->validation->validationToken()->id_client;
        $transaksi = $this->m_transaksi->getTransaksi($id);
        if ($transaksi) {
           $this->response( [
                'status' => true,
                'message' => 'Data Transaksi Berhasi ditemukan',
                'data' => $transaksi
                ], 200);
        } else {
            $this->response( [
                'status' => false,
                'message' => 'Data Transaksi Tidak ditemukan'
                ], 404);
        }
    }

    public function simpan_post()
    {

    	$id_user = $this->input->post('id_user');
    	$id_produk = $this->input->post('id_produk');
    	$produk = $this->db->get_where('tbl_produk',array('id_produk' => $id_produk))->row_array();
    	$produkid = $produk['id_produk'];
    	$qty = $this->input->post('qty');
    	$status = 0;

    	

    	if($produkid == true ){
    		$data= [
                "id_produk" => $produkid,
                'harga' => $produk['harga'],
                'qty' => $qty,
                'id_user' => $id_user,
                'status' => 0
            ];
    		$this->m_transaksi->simpan_barang($data);
    		$this->response( [
			                'status' => true,
			                'message' => 'berhasil disimpan'
		           			 ], 404 );
    	}else{
    		$this->response( [
			                'status' => false,
			                'message' => 'gagal disimpan'
		           			 ], 404 );
    	}

		       
    }


    function hapusitem_delete()
    {
    	$id = $this->delete('id_detail');
    	$id_detail = $this->db->get_where('transaksi_detail',array('id_detail' => $id))->num_rows();
    	//var_dump($id_detail); die;
        //$idClient = $this->validation->validationToken()->id_client;

		        if ($id_detail == false) {
		           	$this->response( [
				                'status' => false,
				                'message' => 'Produk Gagal di di Hapus'
			           			 ], 404 );
	        	}else{

			        $this->m_transaksi->hapus_item($id);

			        $this->response( [
					                'status' => false,
					                'message' => 'Produk Berhasil di Hapus'
				           			 ], 404 );
	    			}    	
    }


    function selesai_belanja_post()
    {
        $tanggal =  date('d-m-y');
        $point = 10;
        $user = $this->input->post('id_user');
        $id_op = $this->db->get_where('tbl_user',array('id_user'=>$user))->row_array();
        $id_user = $this->db->get_where('tbl_point',array('id_user'=>$user))->row_array();

        if($id_user == 0) {
        	$dataP = ['id_user' => $user, 
        				'total_point' => $point
        	];
        	$this->db->insert('tbl_point' ,$dataP);
        }else{
        	$point = $id_user['total_point'];
        	$tpoint = $point + 10;

        	$this->m_transaksi->updatepoin($tpoint);

        }

        if($id_op == true){
        	$data = array('id_user'=>$id_op['id_user'],
        		'tgl_transaksi'=>$tanggal,
        		
    		);
        	$this->m_transaksi->selesai_belanja($data);
        	
        	$this->response( [
		                'status' => true,
		                'message' => 'Terimah kasih telah berbelanja'
	           			 ], 200 );

        }else{
        	$this->response( [
		                'status' => false,
		                'message' => 'Nama anda tidak terdaftar'
	           			 ], 200 );


        }
        
        
    }





}