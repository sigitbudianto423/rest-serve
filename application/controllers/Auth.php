<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;
use \Firebase\JWT\JWT;

class Auth extends RestController {
    function __construct(){
        parent::__construct();
        
        $this->load->model('m_auth');
    }


    public function registerC_post(){
        $data['username'] = $this->input->post('username');
		$data['email'] = $this->input->post('email');
		$password = $this->input->post('password');
		$data['password'] = sha1($password);
		//$rePass = $this->input->post('rePass');

        $this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');
        //$this->form_validation->set_rules('rePass', 'Password konfirmasi', 'required|matches[password]');
        
        if($this->form_validation->run() == FALSE){
            $this->response( [
                'status' => false,
                'message' => 'Mohon isi dulu From di atas'
            ], 404 );
        }

        if($this->m_auth->by_acount($data['email']) > 0){
            $this->response( [
                'status' => false,
                'message' => 'Akun anda telah terdaftar'
            ], 404 );
        }

       
        $idclient = $this->m_auth->create($data);
            $this->response( [
                'status' => true,
                'message' => 'Akun Berhasil ditambahkan'
            ], 200 );
    }

    public function loginC_post(){
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $get_client = $this->m_auth->get_by_email($email);

        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');

        if($this->form_validation->run() === FALSE){
             $this->response( [
                'status' => false,
                'message' => 'Mohon isi dulu From di atas'
            ], 404 );
        }

        if(empty($get_client)){
            $this->response( [
                'status' => false,
                'message' => 'Mohon cek Email dan Password, Silahkan coba lagi'
            ], 404 );
        }
        if ($get_client->password != sha1($password)) {
            $this->response( [
                'status' => false,
                'message' => 'Mohon cek Email dan Password, Silahkan coba lagi'
            ], 404 );
        }


        $key = "example_key";
        $payload = array(
            "iss" => "http://example.org",
            "aud" => "http://example.com",
            "iat" => 1356999524,
            "nbf" => 1357000000,
            "id_client" => $get_client->id_client,
            "username" => $get_client->username,
            "email" => $get_client->email
        );

        $jwt = JWT::encode($payload, $key);

        $this->response( [
                'status' => true,
                'message' => 'Login Berhasil',
                'token'  =>$jwt
            ], 200 );

        $this->session->set_userdata($get_client);


       
    }


	
}