<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;
//use \Firebase\JWT\JWT;

class Produk extends RestController {
    function __construct(){
        parent::__construct();

        $this->load->library('validation');
        $this->validation->validationToken();
        $this->load->model('m_produk');
    }

    public function index_get($id = null){
        
    	$idClient = $this->validation->validationToken()->id_client;
        $Product = $this->m_produk->getProduk($id, $idClient);
        if ($Product) {
           $this->response( [
                'status' => true,
                'message' => 'Produk Berhasi ditemukan',
                'data' => $Product
                ], 200);
        } else {
            $this->response( [
                'status' => false,
                'message' => 'Produk Tidak ditemukan'
                ], 404);
        }
    }

    public function index_post(){

    	$config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $nama_image = $_FILES['gambar']['name'];
        $nama_produk = $this->input->post('nama_produk');
        $harga = $this->input->post('harga');
        $deskripsi = $this->input->post('deskripsi');
        $idClient = $this->validation->validationToken()->id_client;
        

        $this->form_validation->set_rules('nama_produk', 'Nama Produk', 'required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
        $this->form_validation->set_rules('harga', 'Harga', 'required|numeric');
     	$this->form_validation->set_rules('gambar', 'Gambar', 'required');



        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('gambar'))
        {
            $this->response( [
                'status' => false,
                'message' => 'Gambar Gagal di Upload'
            ], 404 );
        }
        else
        {
             $data= [
                "nama_produk" => $nama_produk,
                'harga' => $harga,
                'deskripsi' => $deskripsi,
                'id_client' => $idClient,
                'gambar' => $nama_image
            ];
            $this->db->insert('tbl_produk',$data);
            $this->response( [
                'status' => true,
                'message' => 'Produk berhasil di Upload'
            ], 200 );
        }

    }

	    	public function changeUploadImage($gambar, $gambarLama)
			    {
			    	$config['upload_path']          = './uploads/';
			        $config['allowed_types']        = 'gif|jpg|png';
			        $this->load->library('upload', $config);

			        if( ! $this->upload->do_upload('gambar')) 	{
			        	return $gambarLama;

			        }else{
			        	//unlink($gambarLama);
			        	return $this->upload->data('file_name');
			        }

			    }


     		public function index_put(){

        	 	$this->form_validation->set_data($this->put());

		        $this->form_validation->set_rules('nama_produk', 'Nama Produk', 'required');
		        $this->form_validation->set_rules('harga', 'Harga', 'required|numeric');
		        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
		       

		        if ($this->form_validation->run() == false) {
			            $this->response( [
				                'status' => false,
				                'message' => $this->form_validation->error_array()
			           			 ], 404 );
			        }

			        $id = $this->put('id_produk');
			        $gambar = $this->put('gambar');

			        $idClient = $this->validation->validationToken()->id_client;

		        if ($this->m_produk->cekId($id, $idClient) == 0) {
		           $this->response( [
			                'status' => false,
			                'message' => 'Id Tidak Ditemukan'
		           			 ], 404 );
		        }

			        $cekId = $this->m_produk->getProduk($id, $idClient);			  
        			$gambarLama = $cekId->gambar;

        		 $data = [			       
			            'nama_produk' => $this->put('nama_produk'),
			            'deskripsi' => $this->put('deskripsi'),
			            'harga' => $this->put('harga'),
			            'gambar' => $this->changeUploadImage($gambar, $gambarLama)
			        ];


		         if ( ! $this->m_produk->updateProduct($id, $data) == True) {
		            $this->response( [
			                'status' => true,
			                'message' => 'Produk Berhasil di Edit'
		           			 ], 200);
		        }else{
			        	$this->response( [
				                'status' => false,
				                'message' => 'Produk Gagal di Edit'
			           			 ], 404 );
		        	}
    		}




    		public function index_delete(){
		        
		        $id = $this->delete('id_produk');
		        $idClient = $this->validation->validationToken()->id_client;

		        if ($this->m_produk->cekId($id, $idClient) == 0) {
		           	$this->response( [
				                'status' => false,
				                'message' => 'Produk Gagal di di Hapus'
			           			 ], 404 );
	        	}else{

			        $this->m_produk->deleteProduct($id, $idClient);

			        $this->response( [
					                'status' => false,
					                'message' => 'Produk Berhasil di Hapus'
				           			 ], 404 );
	    			}    	
		    
    		}




   

}