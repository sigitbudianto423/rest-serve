<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_hadiah extends CI_Model
{
  	private $table = "tbl_hadiah";

	public function getHadiah($id, $idClient){
		$this->db->where('id_client', $idClient);
        if ($id == null) {
            $hadiah = $this->db->get($this->table)->result();
        } else {
            $this->db->where('id_hadiah', $id);
            $hadiah = $this->db->get($this->table)->row();
        }
        return $hadiah;
	}

	public function cekId($id, $idClient)
    {
        $data = $this->db->get_where($this->table, ['id_hadiah' => $id, 'id_client' => $idClient]);
        return $data->num_rows();
    }

     public function updateHadiah($id, $data)
    {
        $this->db->where('id_hadiah', $id);
        $this->db->update($this->table, $data);
    }

    public function deleteHadiah($id, $idClient)
    {
        $this->db->where('id_client', $idClient);
        $this->db->where('id_hadiah', $id);
        $this->db->delete($this->table);
    }

}