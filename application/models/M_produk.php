<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_produk extends CI_Model
{
  	private $table = "tbl_produk";

	public function getProduk($id, $idClient){
		$this->db->where('id_client', $idClient);
        if ($id == null) {
            $produk = $this->db->get($this->table)->result();
        } else {
            $this->db->where('id_produk', $id);
            $produk = $this->db->get($this->table)->row();
        }
        return $produk;
	}

	public function cekId($id, $idClient)
    {
        $data = $this->db->get_where($this->table, ['id_produk' => $id, 'id_client' => $idClient]);
        return $data->num_rows();
    }

     public function updateProduct($id, $data)
    {
        $this->db->where('id_produk', $id);
        $this->db->update($this->table, $data);
    }

    public function deleteProduct($id, $idClient)
    {
        $this->db->where('id_client', $idClient);
        $this->db->where('id_produk', $id);
        $this->db->delete($this->table);
    }

}