<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_transaksi extends CI_Model
{

	function getTransaksi($id){
		//$this->db->where('id_client', $idClient);
        if ($id == null) {
            $transaksi = $this->db->get('tbl_transaksi_produk')->result();
        } else {
            $this->db->where('id_transaksi', $id);
            $transaksi = $this->db->get('tbl_transaksi_produk')->row();
        }
        return $transaksi;
	}

	  function simpan_barang($data)
    {
        $this->db->insert('transaksi_detail',$data);
    }


    public function hapus_item($id_detail)
    {
        //$this->db->where('id_client', $idClient);
        $this->db->where('id_detail', $id_detail);
        $this->db->delete('transaksi_detail');
    }


    function selesai_belanja($data)
    {
        $this->db->insert('tbl_transaksi_produk',$data);
        $last_id = $this->db->query("select id_transaksi from tbl_transaksi_produk order by id_transaksi desc")->row_array();
        $this->db->query("update transaksi_detail set id_transaksi='".$last_id['id_transaksi']."' where status='0'");
        $this->db->query("update transaksi_detail set status='1' where status='0'");
    }


    function updatepoin($tpoint){
    	$this->db->update('tbl_point',['total_point'=>$tpoint]);
    }


}