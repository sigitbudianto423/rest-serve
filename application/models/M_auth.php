<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_auth extends CI_Model
{
    private $table = "tbl_client";

    public function by_acount($email){
        $data = $this->db->get_where($this->table , ['email' => $email]);
        return $data->num_rows();
    }

    public function create($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id,$jwt){
        $this->db->where('id_client',$id);
        $this->db->update($this->table,[
            'token' => $jwt
        ]);
    }

     public function get_by_email($email)
    {
        $data = $this->db->get_where($this->table, ['email' => $email]);
        return $data->row();
    }

}